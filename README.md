# Twitter Stream - Real Time Tweets

Streaming tweets with the [Twitter Real Time Api](https://developer.twitter.com/en/docs/tweets/filter-realtime/overview).

## Installation

### Elixir Docker Stack

```bash
git clone https://gitlab.com/exadra37-playground/elixir/docker-stack twitter-stream && cd twitter-stream
```


### Twitter Stream

```bash
git clone https://gitlab.com/exadra37-playground/elixir/twitter-stream app/twitter-stream
```


## How to Use

### Streaming on IEX Shell

On host shell...

```bash
./stack run elixir-iex
```

On IEX shell...

```bash
iex> TwitterStream.filter "word-to-filter"
```

This is how I personally stream tweets about the Elixir and Erlang ecosystem...

```bash
iex> TwitterStream.filter "ElixirLang,MyElixirStatus,ElixirStatus,ElixirPhoenix,Elixir Phoenix,Elixir OTP,Elixir Beam,Erlang,Erlang OTP,Erlang Beam,#Elixir"
```

To stop the the stream just hit `ctrl+c` twice.

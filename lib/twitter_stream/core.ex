defmodule TwitterStream.Core do
  @moduledoc """
  Documentation for TwitterStream.Core
  """
  alias TwitterStream.{TweetParser, CliOutput}
  alias ExTwitter.Model.Tweet

  def track(tag) do
    IO.puts "\nTwitter Stream for Tag(s):\n #{tag}\n"

    ExTwitter.stream_filter([track: tag], :infinity)
    |> Stream.map(fn(tweet = %Tweet{}) -> CliOutput.print_tweet_text(tweet) end)
    |> Enum.to_list()
  end

  def filter(tag) do
    IO.puts "\nTwitter Stream for Tag(s):\n #{tag}\n"

    ExTwitter.stream_filter([track: tag], :infinity)
    |> Stream.map(fn(tweet = %Tweet{}) -> process_stream(tweet) end)
    |> Enum.to_list()
  end

  defp process_stream(tweet = %Tweet{}) do
    tweet
    |> TweetParser.parse_tweet()
    |> CliOutput.print_tweet()
  end
end

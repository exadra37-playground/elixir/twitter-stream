defmodule TwitterStream.Urls do
  defstruct(
    videos: [],
    sites: [],
  )
end

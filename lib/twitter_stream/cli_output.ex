defmodule TwitterStream.CliOutput do
  @moduledoc """
  Documentation for TwitterStream.Parser
  """
  alias TwitterStream.Urls

  alias ExTwitter.Model.Tweet

  def print_tweet({tweet = %Tweet{}, urls = %Urls{videos: videos, sites: sites}})
  when videos !== [] or sites !== [] do

    IO.puts "\n-------------------------------------------\n"

    tweet
    |> print_tweet_author()
    |> print_tweet_text()

    urls
    |> print_videos_urls()
    |> print_sites_urls()

    tweet
  end

  def print_tweet({tweet = %Tweet{}, urls = %Urls{}}) do
    {tweet, urls}
  end

  def print_tweet_author(tweet = %Tweet{}) do
    IO.puts "Author: @#{tweet.user.screen_name}\n"
    tweet
  end

  def print_tweet_text(tweet = %Tweet{}) do
    IO.puts "Tweet:\n#{tweet.text}\n"
    tweet
  end

  defp print_videos_urls(urls = %Urls{videos: videos}) do
    videos
    |> Enum.each(fn url -> IO.puts "Video Url: #{url}" end)

    urls
  end

  defp print_sites_urls(urls = %Urls{sites: sites}) do
    sites
    |> Enum.each(fn url -> IO.puts "Site Url: #{url}" end)

    urls
  end

end

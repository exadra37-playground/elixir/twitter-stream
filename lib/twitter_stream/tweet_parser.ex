defmodule TwitterStream.TweetParser do
  @moduledoc """
  Documentation for TwitterStream.Parser
  """
  alias TwitterStream.Urls

  alias ExTwitter.Model.Tweet

  def parse_tweet(tweet = %Tweet{}) do
    tweet
    |> get_all_urls()
  end

  defp get_all_urls(tweet = %Tweet{}) do

    {tweet, %Urls{}}
    |> get_videos_urls()
    |> get_sites_urls()
  end

  defp get_videos_urls({tweet = %Tweet{extended_entities: %{media: media}}, urls = %Urls{}}) do
    links = media
    |> Enum.map(fn video -> get_video_url(video) end)

    urls = Map.put(urls, :videos, links)

    {tweet, urls}
  end

  defp get_videos_urls({tweet = %Tweet{}, urls = %Urls{}}) do
    {tweet, urls}
  end

  defp get_video_url(video = %{type: "video"}) do
    video.expanded_url
  end

  defp get_video_url(video) do
    ""
  end

  defp get_sites_urls({tweet = %Tweet{entities: %{urls: tweet_urls}}, urls = %Urls{}}) do
    links = tweet_urls
    |> Enum.map(fn url -> url.expanded_url end)

    urls = Map.put(urls, :sites, links)

    {tweet, urls}
  end

  defp get_sites_urls({tweet = %Tweet{}, urls = %Urls{}}) do
    {tweet, urls}
  end

end

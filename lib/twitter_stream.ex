defmodule TwitterStream do
  @moduledoc """
  Documentation for TwitterStream.
  """
  alias TwitterStream.Core

  defdelegate track(tag), to: Core

  defdelegate filter(tag), to: Core

end
